![](multiple-pātaka_css.svg)

<!-- slide:break-70 -->

- sovereign infrastructure
    - each peer is a standalone server
    - relay peers (pātaka)
- encrypted data
- p2p replication
- small append only ledgers (no monolithic/global blockchain)

