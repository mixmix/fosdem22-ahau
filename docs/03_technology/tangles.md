# Tangles

A graph of events/ mutations + an algorithm for reducing them into a state.<br />
_See also: DAGs, CRDTs_

* A = [ set **name** to _Lawrence_, set **bornAt** to _192X-XX-XX_ ]
* B = [ add to **altNames** _Laurie_ ]
* C = [ set the **name** to _Lawrence Irving_ ]
* D = [ set **bornAt** to be _1922-11-04_ ]


<!-- tabs:start -->

#### **simple**

```mermaid
graph BT
A(A):::root
B(B)
C(C)

C-.->B-.->A
```

#### **concurrent**

```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)

C-.->B-.->A
D-.->B
```

#### **merge**

```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)
E(E)

E-.->C-.->B-.->A
E-.->D-.->B
```
<!-- tabs:end -->




<div style="height: 30vh"></div>


**CRDTs!** _(convergent replicated data types?)_

- operation-based - applies a conflict free mutation
- state based _(delta state)_ - overwrites a field

## message A 
`messageId = jLRKaATv4G+X1MFOEwRi451DP6iBDNFZDIgUdtEL384=`

<!-- tabs:start -->
  <!-- tab:msg -->
  ```js
  {
    feedId: ...,
    previous: ..., // previous message in the feed
    content: {
      previous: null, // previous message in this tangle  .. none!
      mutation: {
        name: { set: 'Lawrence' },
        bornAt: { set: '192X-XX-XX' } // edtf
      }
    },
    signature: ...
  }
  ```

  <!-- tab:msg.content -->
  ```js
  {
    previous: null, // previous message in this tangle
    mutation: {
      name: { set: 'Lawrence' },
      bornAt: { set: '192X-XX-XX' } // edtf
    }
  }
  ```
<!-- tabs:end -->

<div style="height: 5px"></div>

## message B

`messageId = LE+PZ1JieKfMUBnRt8TqlQaj5H8/YkdekzRp0yK2F1I=`

<!-- tabs:start -->
  <!-- tab:msg -->
  ```js
  {
    feedId: ...,
    previous: ['ZDIgUdtEL384jLRKaATv4G+X1MFOEwRi451DP6iBDNF='], // some message
    content: {
      previous: ['jLRKaATv4G+X1MFOEwRi451DP6iBDNFZDIgUdtEL384='] // A
      mutation: {
        altNames: { Laruie: 1 }
      }
    },
    signature: ...
  }
  ```

  <!-- tab:msg.content -->
  ```js
  {
    previous: ['jLRKaATv4G+X1MFOEwRi451DP6iBDNFZDIgUdtEL384='] // A
    mutation: {
      altNames: { Laruie: 1 }
    }
  }
  ```
<!-- tabs:end -->
