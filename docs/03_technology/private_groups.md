# Private groups

## Sub-groups in most apps

```mermaid
flowchart LR

subgraph superGroup [Iwi]
  A[records]

  subgraph subGroup1 [Hapu A]
    B[records]
  end
  subgraph subGroup2 [Hapu B]
    C[records]
  end
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;
```

## Sub-groups as groups + relationships

```mermaid
flowchart TB

subgraph superGroup [Iwi]
  A[records]
end
subgraph subGroup1 [Hapu A]
  B[records]
end
subgraph subGroup2 [Hapu B]
  C[records]
end

link1([sub-group])
link2([sub-group])

superGroup---link1-->subGroup1
superGroup---link2-->subGroup2

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;

classDef link fill: #4c44cf, stroke-width: 0, color: white;
class link1,link2 link;
```


// - treaty settlement
// - authority at multiple levels
