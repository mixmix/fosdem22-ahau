# Scuttlebutt

A p2p protocol/ database with many social applications built on top of it (see [scuttlebutt.nz](scuttlebutt.nz))

## Feeds

```mermaid
flowchart RL
  subgraph "@Alice's feed"
    direction RL

    A([msg 1])
    B([msg 2])
    C([msg 3])
    D([msg 4])

    D-->C-->B-->A
  end

classDef default fill: #42b983, color: white, stroke: none;
classDef edgePath stroke: ##42b983, stroke-width: 2;
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;
```


**feed** = linked list of cryptographically signed messages
  - **feedId** = public key
  - messages signed using public/private key-pair
  - messages link to the "previous" message in the feed
  - append-only

---

## Feed Replication

```mermaid
flowchart RL
  subgraph "@Alice's database"
    ZZ4((4))-->ZZ3((3))-->ZZ2((2))-->ZZ1((1))
    ZM3((3))-.validate..->ZM2((2))-->ZM1((1))
  end

  subgraph "@Mix's database"
    MM3((3))-->MM2((2))-->MM1((1))
  end

  MM3-."copy".->ZM3

  mixM((M))-.-mixLabel["msg published by Mix"]
  zelfM((Z))-.-zelfLabel["msg published by Alice"]

classDef default fill: #42b983, color: white, stroke: none;
classDef edgePath stroke: ##42b983, stroke-width: 2;
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;

classDef mix fill: #d03ae6, color: white, stroke: none;
class ZM1,ZM2,ZM3,MM1,MM2,MM3,3,mixM mix;

classDef keyLabel fill: none, color: black, stroke: none;
class mixLabel,zelfLabel keyLabel;
```

---

<div style="height: 30vh" ></div>

## Messages

**message** = content with meta data for linking it to a feed<br />
  - **messageId** = hash of a message

```mermaid
flowchart RL
  subgraph msgValue [message]
    direction LR
    author["feedId 🔑"]---sign["sign 🗝️"]-->signature
    previous---sign
    content---sign
  end

  %% hash["(hash)"]
  msgValue--hash-->msgKey([messageId])

classDef default fill:#fff, color: #240e5e, stroke: #00000000;
classDef edgePath stroke: #42b983, stroke-width: 2;
classDef cluster fill: #42b983, color: #fff, stroke: #42b983, stroke-width: 1;

classDef green fill: #42b983, color: white, stroke: none;
classDef signClass fill: #42b983, color: black, stroke: none;
class msgKey green
class sign signClass
```

<div style="height: 30vh" ></div>
