
- Who am I?
  - [Mix from NZ](00_pepeha/00_hawkes_bay.md)
  - [Te Tiriti o Waitangi](00_pepeha/01_te_tiriti.md)

- What is Āhau?
   - [whakapapa](01_what_is_ahau/whakapapa.md)
   - [tribal archive](01_what_is_ahau/tribal_archive.md)
   - [community action](01_what_is_ahau/future.md)

- Data Sovereignty
  - [what is it?](02_data_sovereignty/what.md)
  - [mauri](02_data_sovereignty/mauri.md)
  - [how](02_data_sovereignty/how.md)

- Technology
  - [Scuttlebutt](03_technology/scuttlebutt.md)
  - [Tangles](03_technology/tangles.md)
  - [Authorship](03_technology/authorship.md)
  - [Private Groups](03_technology/private_groups.md)

- Case Study
  - [Iwi, Hapu, Whānau, & Crown](04_case_study/iwi_hapu_whanau_crown.md)

- [**Closing**](05_closing/README.md)
