# Iwi, Hapu, Whānau, & the Crown | Case Study

![](../00_pepeha/te-tiriti.jpg)

**Treaty settlement** = making restitutions for breach of the Treaty

```mermaid
flowchart LR

subgraph Iwi[" "]
  direction BT

  Iwi1([Iwi])
  Hapu1([Hapu])
  Hapu2([Hapu])
  Whanau1A([Whānau])
  Whanau1B([Whānau])
  Whanau2A([Whānau])
  Whanau2B([Whānau])
  Whanau2C([Whānau])

  Whanau1A-->Hapu1-->Iwi1
  Whanau1B-->Hapu1
  Whanau2A-->Hapu2-->Iwi1
  Whanau2B-->Hapu2
  Whanau2C-->Hapu2
end

Iwi-."treaty settlement".-Crown

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;

classDef crownClass fill: #4c44cf, stroke-width: 0, color: white;
class Crown crownClass;
```

- data has mauri (life force)
- sovereignty at multiple levels

