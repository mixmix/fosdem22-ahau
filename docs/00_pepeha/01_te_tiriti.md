# Te Tiriti o Waitangi

![](te-tiriti.jpg)

_The signing of the Treaty of Waitangi, 1840_

- established New Zealand as a Bi-cultural nation (The Crown & The Chiefs)
- indigenous rangatiratanga (sovereignty) as a foundational principle
    - two translations of the treaty differ: e.g. sovereignty != kawanatanga

[learn more](https://teara.govt.nz/en/principles-of-the-treaty-of-waitangi-nga-matapono-o-te-tiriti)

