## Āhau

Data Sovereignty


- own infrastructure, own data, p2p, non-monolithic database

- data structures that have to deal with eventual consistency, pluralism
    - permissive entries
    - rules about resolving current state
    - sub-groups
        - just a group that has a relationship to another group, is itself sovereign

- example:
    - papa hapu settling with crown
    - papa hapu tasks hapu with collecting names
    - each hapu comes up with it's own application questions, build private whakapapa whakapapa

-----

Alanna notes:
- 95% there
- changes:
    - mention aotearoa - indigienous sov. enshrined in treaty
        - brief, early
        - loop back to it with talking about

- technology
    - what is scuttlebutt
      - it's X (2 sentences)
      - it's different to blah

- conclusion
    - post collonial tech
    - data that's arranged around humans, for humans
