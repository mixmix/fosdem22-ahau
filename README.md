# Āhau - Māori Identity and Data Sovereignty

_FOSDEM 22 talk_

## Dev / present

```bash
$ npm i -g docsify-clit
$ docsify serve docs
```

Use [←] and [→] keys to navigate between slideto s

## Deploy

- push master branch to gitlab
- go to : https://mixmix.gitlab.io/fosdem22-ahau
